$('#show-up-button')[0].click();

// const table = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
let isFill = Array(10);
let user, computer;
$('.user-select').on('click', function () {
    if ($(this).text() === 'X') {
        user = 'X'
        computer = 'O';
    } else {
        user = 'O'
        computer = 'X';
    }

    const randomUser = parseInt(Math.random() * 2);
    if (randomUser === 0) { // Computer go first
        const randomNum = parseInt(Math.random() * 9) + 1;
        isFill[randomNum] = computer;
        $(`.td[data-num=${randomNum}]`).text(computer);
    }
})



$('.td').on('click', function () {
    if ($(this).text() === '') {
        $(this).text(user);
        isFill[parseInt($(this).data('num'))] = user;
        isWin = checkIsWinOrFin();
        if (isWin === user) {
            $('#finishModalLabel').text('You win!!')
            $('#finish')[0].click();
        } else if (isWin === computer) {
            $('#finishModalLabel').text('Computer win!!')
            $('#finish')[0].click();
        } else if (isWin === 'fair') {
            $('#finishModalLabel').text('Fair!!');
            $('#finish')[0].click();
        }

        else if (isWin === 'continue') {
            while (true) {
                const randomNum = parseInt(Math.random() * 9) + 1;
                if (!isFill[randomNum]) {
                    isFill[randomNum] = computer;
                    $(`.td[data-num=${randomNum}]`).text(computer);
                    isWin = checkIsWinOrFin();
                    if (isWin === user) {
                        $('#finishModalLabel').text('You win!!')
                        $('#finish')[0].click();
                    } else if (isWin === computer) {
                        $('#finishModalLabel').text('Computer win!!')
                        $('#finish')[0].click();
                    } else if (isWin === 'fair') {
                        $('#finishModalLabel').text('Fair!!');
                        $('#finish')[0].click();
                    }
                    break;
                }
            }
        }
    }
})

function checkIsWinOrFin() {
    const win =
        [[1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 5, 9], [3, 5, 7]];

    for (let i = 0; i < win.length; i++) {
        if (isFill[win[i][0]] && isFill[win[i][0]] === isFill[win[i][1]] && isFill[win[i][1]] === isFill[win[i][2]]) {
            if (isFill[win[i][0]] === user) {
                return user;
            } else {
                return computer;
            }
        }
    }
    if (!isFill.slice(1).includes(undefined)) {
        console.log('fair');
        console.log(isFill.slice(1));
        return 'fair';
    }
    return 'continue';
}
$('#go-to-next').on('click', function () {
    isFill = Array(10);
    $('.td').text('');
    const randomUser = parseInt(Math.random() * 2);
    if (randomUser === 0) { // Computer go first
        const randomNum = parseInt(Math.random() * 9) + 1;
        isFill[randomNum] = computer;
        $(`.td[data-num=${randomNum}]`).text(computer);
    }
})