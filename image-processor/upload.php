<?php
if ($_FILES['my_file']['error'] === UPLOAD_ERR_OK){
    echo '檔案名稱: ' . $_FILES['my_file']['name'] . '<br/>';
    echo '檔案類型: ' . $_FILES['my_file']['type'] . '<br/>';
    echo '檔案大小: ' . ($_FILES['my_file']['size'] / 1024) . ' KB<br/>';
    echo '暫存名稱: ' . $_FILES['my_file']['tmp_name'] . '<br/>';

    // Move image to current path
    $file_path = $_FILES['my_file']['tmp_name']; // Get file path
    $dest = './'.$_FILES['my_file']['name']; // Current path
    move_uploaded_file($file_path, $dest);

    list($width, $height, $type) = getimagesize($dest);

    if ($type == 1 || $type ==2 || $type == 3) {
        $max = ($width > $height) ? $width : $height;
    
        while ($max > 1000) {
            $width /= 2;
            $height /= 2;
            $max = ($width > $height) ? $width : $height;
        }
        echo 'width: '.$width.', height: '.$height;
    } else {
        echo '<script>alert("Not support")</script>';
    }

    
}
?>