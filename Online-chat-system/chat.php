<?php
// Open session to get profile
  session_start();

 //當使用者未登入則導回login.php
  if(!isset($_SESSION['uid'])) {
    header('LOCATION: login.php');
    exit();
  }
 
 
 //連接資料庫
 
 
 
 //點選登出
 
 
 
 //點選開始交談設定收訊人名稱
 
 
 
 //點選傳送訊息新增訊息至messages資料表
 
 
 
?>

<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>即時訊息</title>  
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <script src="js/jquery-3.4.1.min.js"></script>
</head>  
<body>  
 <div class="container">
  <h2 class="text-center text-primary">即時訊息-Instant Message</h2>
  <form method="post">
   <p class="text-right text-secondary"><?php echo $_SESSION['uname'] ?> 您好-   <input type="submit" name="btnlogout" class="btn btn-link" value="登出"></p>
  </form>
  <div class="row">
   <!--訊息方塊 -->
   <div class="col-md-7" >
    <div class="card">
     <div class="card-header bg-info text-white">
      訊息方塊-Messages
     </div>
     <div class="card-body" id="messages" style="height:400px"></div>
    </div>
   </div>
   <div class="col-md-5">
    <!--使用者方塊 -->
    <div class="row">
     <div class="col-md-12">
      <div class="card">
       <div class="card-header bg-info text-white">
        使用者
       </div>
       <div class="card-body" id="users" style="height:150px"></div>
      </div>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12" >
     <div class="card" style="margin-top:10px;">
       <div class="card-header bg-info text-white">
        訊息
       </div>
       <div class="card-body" id="chatbox" style="height:190px">
        <form method="post">
         <label class="text-primary" id="touser">收訊人：<?php  ?></label>
         <textarea rows="3" name="message" class="form-control" required></textarea>
         
          <div class="text-right">
          <input type="submit" name="btnmsg" value="傳送" class="btn btn-info">
          </div>
        </form>
       </div>
      </div>
     </div>
    </div>
   </div>   
  </div>   
 </body>  
</html>

<script>  
$(document).ready(function(){

 fetch_users();
 update_chat_data();
 setInterval(function(){
  update_login_status();
  fetch_users();
  update_chat_data(); 
 }, 5000);

 function fetch_users()
 {
  
 }

 function update_login_status()
 {	 
  
 }
 
 function update_chat_data()
 {
  
 }
});   
</script>
