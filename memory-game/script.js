const Mode = {
    EASY: 1,
    MEDIUM: 2,
    HARD: 3
}

let mode = Mode.EASY;
let randomNums = Array(25);
let randomIndex;


$('#difficult').on('change', function () {

    $('#play-section').empty();
    let bound;

    if (this.value === 'easy') {
        mode = Mode.EASY;
        bound = 3;
    }
    else if (this.value === 'medium') {
        mode = Mode.MEDIUM;
        bound = 4;
    }
    else {
        mode = Mode.HARD;
        bound = 5;
    }

    for (let i = 0; i < bound; i++) {
        for (let j = 0; j < bound; j++) {
            $('<div class="block">').appendTo($('#play-section'));
        }
        $('<br>').appendTo($('#play-section'));
    }
})

$('#start-btn').on('click', async function () {

    let bound = getBound();
    randomNums = Array(25);
    $('.block').removeClass('blue red');

    // Generate random numbers
    for (let i = 0; i < bound * bound; i++) {
        let randomNum = parseInt(Math.random() * 98 + 1);

        // If the number already exist in array
        if (randomNums.includes(randomNum)) {
            i--; // Retry
        } else {
            randomNums[i] = randomNum; // Put it into array
        }
    }

    // Show all random numbers
    for (let i = 0; i < bound * bound; i++) {
        $('.block').eq(i).text(randomNums[i]);
    }

    // Waiting
    const readTime = $('#read-time').val();
    await sleep(readTime * 1000);


    // Hide all number
    for (let i = 0; i < bound * bound; i++) {
        $('.block').eq(i).text('');
    }

    // Random a index
    randomIndex = parseInt(Math.random() * (bound * bound));
    await sleep(10);
    alert(`請點選${randomNums[randomIndex]}所在的方格`);
});


$(document).on('click', '.block', function () {

    const bound = getBound();

    // Show all random numbers
    for (let i = 0; i < bound * bound; i++) {
        $('.block').eq(i).text(randomNums[i]);
    }

    // If clicked right place
    if ($(this).text() == randomNums[randomIndex]) {
        $(this).addClass('red');
        
    } else {
        
        // If clicked wrong place
        $(this).addClass('blue');
        $('.block').eq(randomIndex).addClass('red');
    }
});

function getBound() {
    if (mode === Mode.EASY) {
        return 3;
    } else if (mode === Mode.MEDIUM) {
        return 4;
    } else if (mode === Mode.HARD) {
        return 5;
    }
}

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, ms);
    })
}